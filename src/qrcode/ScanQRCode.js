import React, {Component} from 'react';
import {StyleSheet, Text, View,Alert} from 'react-native';
import FadeInView from '../common/FadeInView';
import Button from '../common/Button';
import BackToMenuView from '../common/BackToMenuView';
import QRCodeScanner from 'react-native-qrcode-scanner';

export default class ScanQRCode extends Component {
	constructor(props) {
		super(props);

		this.state = {
			qrvalue: null
		}
	}

	onSuccess(e) {
		this.setState({qrvalue: e.data});
		Alert.alert(e.data);
	}

    render() {
      return (
        <BackToMenuView buttonText="Menu" onPress={this.props.menuSelect('menu')} title="Scan QR Code">
			<FadeInView style={styles.container} duration={500}>	
			{this.state.qrvalue && <Text>The QR Code says: {this.state.qrvalue}</Text>}
			
			<QRCodeScanner
				onRead={this.onSuccess.bind(this)}
				topContent={
					<Text style={styles.scannerText}>
						Scan a QR Code
					</Text>
        		}
        		bottomContent={
					<Button style={styles.buttonStyle}>Got It</Button>
        		}
      		/>
			</FadeInView>
        </BackToMenuView>
      );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  scannerText: {
    fontFamily: 'Nunito',
    fontSize: 20,
    textAlign: 'center'
  },
  buttonStyle: {
	  width: 300,
	  margin: 10
  }
});