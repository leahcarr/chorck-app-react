import React, {Component} from 'react';
import {StyleSheet, TextInput, Alert} from 'react-native';
import FadeInView from '../common/FadeInView';
import BackToMenuView from '../common/BackToMenuView';
import QRCode from 'react-native-qrcode';

export default class GenerateQRCode extends Component {
	constructor(props) {
		super(props);
		this.state = {
			text: ''
		}
		this.handlePress = this.handlePress.bind(this);
	}
	handlePress() {

	}
	render() {
      return (
        <BackToMenuView buttonText="Menu" onPress={this.props.menuSelect('menu')} title='Generate QR Code'>
			<FadeInView style={styles.container} duration={500}>
				<QRCode
          			value={this.state.text}
          			size={200}
         			bgColor='purple'
          			fgColor='white'
				/>
				<TextInput
					style={styles.inputStyle}
					placeholder="Type in the string you want to encode"
					onChangeText={(text) => this.setState({text})}
       				value={this.state.text}
				/>
			</FadeInView>
        </BackToMenuView>
      );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  inputStyle: {
	marginTop: 50
  },
  brandText: {
    fontFamily: 'Nunito',
    fontSize: 50,
    textAlign: 'center'
  }
});