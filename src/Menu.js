import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import FadeInView from './common/FadeInView';
import Button from './common/Button';

export default class Menu extends Component {
    render() {
      return (
        <View style={styles.container}>
			<FadeInView style={styles.container} duration={500}>
        	<Button style={styles.buttonStyle} onPress={this.props.menuSelect('generateQRCode')}>Generate QR Code</Button>
			  	<Button style={styles.buttonStyle} onPress={this.props.menuSelect("scanQRCode")}>Scan QR Code</Button>
			</FadeInView>
        </View>
      );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  brandText: {
    fontFamily: 'Nunito',
    fontSize: 50,
    textAlign: 'center'
  },
  buttonStyle: {
	  width: 300,
    margin: 10,
    backgroundColor: '#0055B7',
    borderRadius: 5
  }
});