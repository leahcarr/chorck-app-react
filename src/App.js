/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';
import Splash from './common/Splash';
import Menu from './Menu';
import AppView from './common/AppView';
import GenerateQRCode from './qrcode/GenerateQRCode';
import ScanQRCode from './qrcode/ScanQRCode';



type Props = {};
  export default class App extends Component<Props> {
    constructor(props) {
      super(props);
      this.state = {
        ready: false,
        currentView: 'menu'
      };
      this.menuSelect = this.menuSelect.bind(this);
    }
    componentDidMount() {
      var parent = this;
      setTimeout(() => {
        parent.setState({ready: true});
      },3000);
    }
    menuSelect(currentView) {
      const parent = this;
      return function(e) {
        parent.setState({currentView: currentView});
      };
    }

    render() {
      let {currentView} = this.state;
      if(!this.state.ready) {
        return (
          <Splash duration={3000} />
        );
      }
      else {
        if( currentView === 'menu') {
          return (
            <AppView title="Chorck Menu">
              <Menu menuSelect={this.menuSelect} />
            </AppView>
          );
        } else if( currentView === 'generateQRCode') {
          return (
            <AppView title="Generate QR Code">
              <GenerateQRCode menuSelect={this.menuSelect} />
            </AppView>
          );
        } else if ( currentView === 'scanQRCode') {
            return (
              <AppView title="Scan QR Code">
                <ScanQRCode menuSelect={this.menuSelect} />
              </AppView>
            );
        } else {
          return (
            <AppView title="Chorck Menu">
             <Menu menuSelect={this.menuSelect} />
            </AppView>
          );
        }
      }
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  brandText: {
    fontFamily: 'Nunito',
    fontSize: 50,
    textAlign: 'center'
  }
});
