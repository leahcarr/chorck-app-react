import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default class AppView extends Component {
    render() {
      return (
        <View style={styles.wrapper}>
			<Text style={styles.header}>{this.props.title}</Text>
			{this.props.children}
        </View>
      );
    }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
	backgroundColor: '#F5FCFF',
	width: '100%'
  },
  header: {
	  marginTop: 50,
	  fontFamily: 'Nunito',
	  fontSize: 30,
	  textAlign: 'left'
  }
});