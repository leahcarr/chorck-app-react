import React, {Component} from 'react';
import {Animated, Platform, StyleSheet, Text, View, Image, StatusBar} from 'react-native';

export default class ProgressLoading extends Component {
	state = {
		fill: new Animated.Value(0),  // Initial value for opacity: 0
	  }
	componentDidMount() {
		Animated.timing(                  // Animate over time
			this.state.fill,            // The animated value to drive
			{
			  toValue: 200,
			  delay: this.props.delay,               // Animate to opacity: 1 (opaque)
			  duration: this.props.duration,              // Make it take a while
			}
		).start();
	}	
	
	render() {
	  let { fill } = this.state;
      return (
        <View style={styles.container}>
			<Animated.View style={{...styles.filler,width: fill }} />
		</View>
      );
    }
}

const styles = StyleSheet.create({
  container: {
	height: 5,
	width: 200,
	borderColor: 'black',
	borderStyle: 'solid',
	borderWidth: 1,
	borderRadius: 5,
	overflow: 'hidden'
  },
  filler: {
		backgroundColor: '#0055B7',
		height: 5,
		borderRadius: 3
  }
});