import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, StatusBar} from 'react-native';
import ProgressLoading from './ProgressLoading';
import FadeInView from './FadeInView';

export default class Splash extends Component {
    render() {
      return (
        <View style={styles.container}>
          <FadeInView style={styles.container} duration={1000}>
			<Image source={require('../../assets/favicon-180x180.png')} />
			<Text style={styles.brandText}>CHORCK</Text>
		 	<ProgressLoading duration={this.props.duration - 1000} delay={1000}/>
		  </FadeInView>
        </View>
      );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  brandText: {
    fontFamily: 'Nunito',
    fontSize: 50,
    textAlign: 'center'
  }
});