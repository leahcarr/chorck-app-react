import React, {Component} from 'react';
import {TouchableOpacity,StyleSheet, Text} from 'react-native';

export default class Button extends Component {
	render() {
      return (
		<TouchableOpacity style={{...this.props.style,...styles.buttonStyle}} onPress={this.props.onPress}>
			{typeof(this.props.children) === 'string'
			? <Text style={styles.textStyle}>{this.props.children}</Text>
			: this.props.children}
		</TouchableOpacity>
      );
    }
}

const styles = StyleSheet.create({
	buttonStyle: {
		opacity: 0.9,
		padding: 10,
	},
	textStyle: {
		fontFamily: 'Nunito',
		fontSize: 15,
		color: 'white',
		textAlign: 'center'
	}
});