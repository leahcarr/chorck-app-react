import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Button from './Button';

export default class BackToMenuView extends Component {
    render() {
      return (
        <View style={styles.wrapper}>

			<View style={styles.container}>
				{this.props.children}
			</View>
			<Button style={styles.buttonStyle} onPress={this.props.onPress}>{'< ' + this.props.buttonText}</Button>
        </View>
      );
    }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
	backgroundColor: '#F5FCFF',
	width: '100%'
  },
  container: {
	flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF', 
  },
  buttonStyle: {
	  width: '100%',
	  margin: 0,
	  backgroundColor: 'grey'
  }
});